<?php

/**
 * @file
 *   Contain the example class for delivery leads.
 */
class exampleLcAction extends leadsCaptureAction {

  protected function preprocessData() {
    foreach($this->data as $key => &$value){
      $value = $key . ': '. $value;
    }
  }

}

<?php

/**
 * @file
 * Hooks provided by LeadsCapture module.
 */
/**
 * @addtogroup hooks
 * @{
 */

/**
 * Implements hook_leadscapture_settings().
 * 
 * Create settings page form for custom module settings.
 * @return array 
 *  keyed by module name and contain keys:
 *   - title - the title of page,
 *   - form - the form callback function name.
 */
function hook_leadscapture_settings() {
  $settings['mymodule'] = array(
    'title' => t('My module settings'),
    'form' => 'mymodule_admin_settings_form',
  );
  return $settings;
}

/**
 * Implements hook_leadscapture_client_info().
 * 
 * Regiter the clients for delivering leads.
 * @return array 
 *   keyed by delivery name (you can use your module name) and contain:
 *   - name - delivery client title.
 *   - description - delivery client description
 *   - handler - class name for delivery handler. This class should be 
 *      implements leadCaptureDelivery.
 */
function hook_leadscapture_client_info() {
  $client = array();
  $client['mymodule'] = array(
    'name' => t('MyModule delievery client'),
    'description' => t('Client for delivery messages'),
    'handler' => 'myDeliveryHandler',
  );
  return $client;
}

/**
 * Implements hook_leadscapture_action_info().
 * 
 * Regiter the actions for triggering leads input.
 * You can trigger the action on form feedback subittion or shopping cart 
 * completion.
 * 
 * @return array 
 *   keyed by action name (you can use your module name) and contain:
 *   - name - action title.
 *   - description - delivery client description
 *   - handler - (optional) class name for delivery handler. This class should be 
 *      implements leadsCaptureAction.
 */
function hook_leadscapture_action_info() {
  $actions = array();
  $actions['mymodule'] = array(
    'name' => t('MyModule submittion action'),
    'description' => t('Action, tirggered when my conditions is true.'),
  );
  return $actions;
}

/*
 * @} End of "addtogroup hooks".
 */

<?php

class leadsCaptureAction {

  protected $context;
  protected $actionName;
  protected $data;
  protected $handler;
  protected $handlerClass = 'leadsCapture';

  public function __construct($actionName, $data, $context = array()) {
    $this->actionName = $actionName;
    $this->context = $context;
    $this->setData($data);
  }
  public function getData(){
    return $this->data;
  }
  public function getContext(){
    return $this->context;
  }
  protected function setData($data) {
    $this->data = $data;
    $this->preprocessData();
  }

  protected function preprocessData() {
    // Action over $this->data.
  }

  protected function prepaper() {
    $this->getHandler();
    // Action over $this->data.
  }

  protected function setHandlerClass($handlerClass) {
    $this->handlerClass = $handlerClass;
  }

  private function getHandler() {
    $className = $this->handlerClass;
    if (!class_exists($className)) {
      watchdog('leadsCapture', 'Leads Capture main handler !handler in action not found', array('!handler' => $className), WATCHDOG_ERROR);
      $this->handler = FALSE;
    }
    else {
      $this->handler = new $className();
    }
  }

  public function getActionName(){
    return $this->actionName;
  }
  public function capture() {
    $this->prepaper();
    if (!$this->handler) {
      return;
    }
    $this->handler->triggerAction($this);
  }

}

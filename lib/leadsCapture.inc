<?php

class leadsCapture {

  protected $deliveryClients;

  public function __construct() {
    $this->deliveryClients = array();
    $clients = leadscapture_get_delievery_clients();

    foreach ($clients as $client_name => $client) {
      $clientClassName = $client['handler'];
      if (class_exists($clientClassName)) {
        $this->deliveryClients[$client_name] = $clientClassName;
      }
    }
  }

  private function checkDeliveryClients($actionName) {
    $relations = variable_get('leadscapture_action_delivery_relations');
    if (is_null($relations)) {
      return;
    }
    if (empty($relations[$actionName])) {
      $this->deliveryClients = array();
      return;
    }
    $this->deliveryClients = array_intersect_key($this->deliveryClients, $relations[$actionName]);
  }

  public function triggerAction($action) {
    $this->checkDeliveryClients($action->getActionName());

    foreach ($this->deliveryClients as $deliveryClient) {
      $deliveryClient::run($action);
    }
  }

}

<?php

/**
 * @file
 *   Contain the example class for delivery leads.
 */
class exampleLcDelivery extends leadCaptureDelivery {

  public static function run($actionHandler) {
    drupal_set_message(implode(", ", $actionHandler->getData()));
  }

}

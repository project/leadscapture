=== English ===

INTRODUCTION
------------
Comming soon

REQUIREMENTS
------------
Comming soon

RECOMMENDED MODULES
-------------------
Comming soon

INSTALLATION
------------
Comming soon

CONFIGURATION
-------------
Comming soon

TROUBLESHOOTING
---------------
Comming soon

FAQ
---
Comming soon

MAINTAINERS
-----------
Comming soon

=== Russian ===

ВВЕДЕНИЕ
--------
Модуль для захвата лидов и распределения по разным каналам доставки.


ЗАВИСИМОСТИ
-----------
Нет зависимостей от других модулей 

РЕКОМЕНДУЕМЫЕ МОДУЛИ
--------------------
lc_telegram - Модуль для отправки лидов в телеграм чат или канал.
lc-webform - модуль захвата лидов с отправленных вебформ(webform).

УСТАНОВКА
---------
Как обычно

НАСТРОЙКА
---------
Пока нет никаких настроек.

РЕШЕНИЕ ПРОБЛЕМ
---------------
Как появятся решим.

ЧАСТЫЕ ВОПРОСЫ
--------------
Задвавйие.

РАЗРАБОТЧИКИ
------------
Gevorg Mkrtchyan

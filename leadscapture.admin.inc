<?php

/**
 * @file
 *   Contain administration pages callbacks.
 */
function leadscapture_settings_form($form, $form_state) {
  $settings = variable_get('leadscapture_action_delivery_relations');

  $actions = leadscapture_get_actions();
  $clients = leadscapture_get_delievery_clients();
  $delivery_options = array();
  foreach ($clients as $client_name => $client) {
    $delivery_options[$client_name] = $client['name'];
  }
  if (is_null($settings)) {
    $default = drupal_map_assoc(array_keys($delivery_options));
  }
  else {
    $default = array();
  }

  foreach ($actions as $action_name => $action) {
    $form[$action_name] = array(
      '#type' => 'checkboxes',
      '#title' => $action['name'],
      '#options' => $delivery_options,
      '#default_value' => isset($settings[$action_name]) ? $settings[$action_name] : $default,
    );
  }
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#value' => t('Save'),
    '#type' => 'submit',
  );
  return $form;
}

/**
 * Save settings to variables.
 */
function leadscapture_settings_form_submit(&$form, &$form_state) {
  form_state_values_clean($form_state);
  $values = array_filter(array_map('array_filter', $form_state['values']));
  variable_set('leadscapture_action_delivery_relations', $values);

  drupal_set_message(t('The configuration options have been saved.'));
}
